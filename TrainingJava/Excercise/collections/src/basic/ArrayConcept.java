package basic;

public class ArrayConcept {
	public static void main (String[] args) {
		int [] age = {2205, 19, 12, 23, 24, 25};
		
	System.out.println(age[1]);
	System.out.println(age.length);
	
	String [] namesofStudents = new String [10];
	namesofStudents[0]= "kimie";
	namesofStudents[1]= "iman";
	namesofStudents[2]= "danial";
	
	System.out.println(namesofStudents[2]);
	
	String [] cities = {"london","paris","malaysia"};
	System.out.println(cities[1]);
	}
}

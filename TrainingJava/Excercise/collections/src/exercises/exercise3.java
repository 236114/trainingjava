package exercises;
import java.util.Arrays;
//remove duplicate elements in an array
public class exercise3 {
	public static int removeDuplicateElements(int arr[], int n){  //temporary array
        if (n==0 || n==1){  
            return n;  
        }  
        int[] rem = new int[n];  
        int j = 0;  
        for (int i=0; i<n-1; i++){  
            if (arr[i] != arr[i+1]){  
                rem[j++] = arr[i];  
            }  
         }  
        rem[j++] = arr[n-1];     
        // Changing original array  
        for (int i=0; i<j; i++){  
            arr[i] = rem[i];  
        }  
        return j;  
    }  
       
    public static void main (String[] args) {  
        int arr[] = {10000,70,30,90,20,20,30,40,70,50000,10};//unsorted array list

		 Arrays.sort(arr);//sorting array  
	        int length = arr.length;  
	        length = removeDuplicateElements(arr, length); 
	
	        //printing array elements
		 for (int i=0; i<length; i++)  
	           System.out.print(arr[i]+" ");  
	}

}

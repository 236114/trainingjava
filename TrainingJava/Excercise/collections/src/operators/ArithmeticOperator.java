package operators;

public class ArithmeticOperator {
	public static void main (String[] args) {
		//int a=10; //declared and assigned value to variable 'a'
		// + - * / %
		int a=10, b=20;
		
		System.out.println("addition operator : "+(a+b)); //addition operator
		System.out.println("subtraction operator : "+(a-b));
		System.out.println("multiplication operator : "+(a*b));
		System.out.println("division operator : "+(a/b));
		System.out.println("modulus operator : "+(b%a));
	}

}

package operators;

public class LogicalOperator {
	
	public static void main(String[] args) {
		//AND&& OR|| NOT!
		//&& - both condition should be true
			System.out.println((100>50)&&(20>10));
			
		//||OR - anyone condition is true then its true
			System.out.println((10>5)||(10>12));
	}

}

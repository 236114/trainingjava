package operators;

public class IncrementDecrement {
	//increment operator ++ ..... decrement operator --
	public static void main(String[] args) {
		int i=100;
		int j=200;
		System.out.println(i); //i value 100
		System.out.println(++i); //100+1=101
		System.out.println(--i); //101-1=100
		System.out.println(++j); //101-1=100
		System.out.println(--j); //101-1=100
	}

}

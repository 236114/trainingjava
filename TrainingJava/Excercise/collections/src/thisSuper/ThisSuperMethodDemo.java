package thisSuper;

class Parent{
	void play() {
		System.out.println("parent play carroms..");
	}
}

public class ThisSuperMethodDemo extends Parent{
	void play () {
		System.out.println("i playy tennis..currents call");
	}
	void all() {//calling current class play method
		
		super.play();//current class play method
		this.play();//parent class play method
		
		
	}

	public static void main(String[] args) {
		ThisSuperMethodDemo obj = new ThisSuperMethodDemo();
		obj.all();

	}

}

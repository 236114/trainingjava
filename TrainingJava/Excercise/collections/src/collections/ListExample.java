package collections;

import java.util.ArrayList;
import java.util.List;


public class ListExample {

	public static void main(String[] args) {

		//creating list
		
			List<String> list = new ArrayList <>();
			
		//adding element to list	
			list.add("Mango");
			list.add("Apple");
			list.add("Banana");
			list.add("Grapes");
			
			
			
			System.out.println(list);
	}

}

package conditionStatement;

import java.util.Scanner;

public class IfElseDemo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the age of candidate: ");
		int age =	sc.nextInt();

		
		if(age>18 && age<100) { //condition is true so this block of code is going execute
			System.out.println("ok this guy is eligible to vote..");
		}
		
		else if (age>100) {
			System.out.println("too old, you are not eligible to vote..");
		}
		
		else {
			System.out.println("you are not eligible to vote..");
		}
	}

}

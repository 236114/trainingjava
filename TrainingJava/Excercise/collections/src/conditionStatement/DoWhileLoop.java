package conditionStatement;

public class DoWhileLoop {

	public static void main(String[] args) {

		int a=1;
		do {
			System.out.println("Print value a: "+a);
			a++;
		}
		while(a<=10);
	}

}

package polymorphism;

//Ovveriding method
//creating parent class
	class vehicle{
		void run() {
			System.out.println("Vehicle is running");
		}
	}
	//creating a child class

	public class Bike2 extends vehicle{
		
	void run() {
		System.out.println("Bike is running safely");
	}
	public static void main(String[] args) {
		Bike2 obj = new Bike2();
		obj.run();
	}

}
